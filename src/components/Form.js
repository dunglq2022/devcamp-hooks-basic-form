import { useEffect, useState } from "react";

function FormBasic () {
    const [firtName, setFirtsName] = useState(localStorage.getItem('firtName'));
    const [lastName, setLastName] = useState(localStorage.getItem('lastName'));

    const handleFirtNameChange = (event) => {
        console.log(event.target.value);
        setFirtsName(event.target.value)
    }

    const handleLastNameChange = (event) => {
        console.log(event.target.value);
        setLastName(event.target.value)
    }

    useEffect(() => {
        localStorage.setItem('firtName', firtName)
        localStorage.setItem('lastName', lastName)
    })

    return (
        <div className="container">
            <h1>Form Basic</h1>
            <div style={{backgroundColor: '#1b2634', width: 250, height: 200}}>
                <div className="col pt-4 ms-4">
                    <input onChange={handleFirtNameChange} value={firtName} style={{backgroundColor: '#8bdfeb', color: "#1b2634"}} placeholder="Devcamp"></input>
                </div>
                <div className="col mt-2 ms-4">
                    <input onChange={handleLastNameChange} value={lastName} style={{backgroundColor: '#8bdfeb', color: "#1b2634"}} placeholder="user"></input>
                </div>
                <h2 className="mt-4 text-center" style={{color: '#8bdfeb'}}>{firtName} {lastName}</h2>
            </div>
        </div>
    )
}
export default FormBasic;